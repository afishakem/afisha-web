interface ITheme {
    [name: string]: {
        [name: string]: string | number | boolean;
    };
}

export const theme: ITheme = {
    base: {},
};

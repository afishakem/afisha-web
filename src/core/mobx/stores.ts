import { Store } from '~/core/mobx';

interface IStores<T> {
    [name: string]: T;
}

export const stores: IStores<Store> = {};

import * as React from 'react';
import { Provider } from 'mobx-react';
import { ThemeProvider } from 'styled-components';

import { stores } from '~/core/mobx';
import { theme } from '~/core/theme';

/**
 * @interface IState
 */
interface IState {
    loaded: boolean;
}

export class App extends React.Component<{}, IState> {
    public state = {
        loaded: false,
    };

    public componentDidMount() {
        this.setState({ loaded: true });
    }

    public render(): React.ReactNode {
        return (
            <Provider {...stores}>
                <ThemeProvider theme={theme}>
                    <h1>Hello</h1>
                </ThemeProvider>
            </Provider>
        );
    }
}

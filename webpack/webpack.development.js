const webpack = require('webpack');
const path = require('path');
const withBase = require('./withBase');

module.exports = withBase({
    mode: 'development',
    entry: [
        'react-hot-loader/patch',
    ],
    output: {
        filename: 'bundle.js',
    },
    devtool: 'eval-source-map',
    devServer: {
        contentBase: path.resolve('dist'),
        host: '0.0.0.0',
        port: process.env.PORT || 8081,
        historyApiFallback: true,
        inline: true,
        noInfo: true,
        hot: true,
        progress: true,
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify('development')
            }
        })
    ],
});
const path = require('path');
const merge = require('webpack-merge');

const HtmlPlugin = require('html-webpack-plugin');

module.exports = (config) => merge(config, {
    entry: [
        path.resolve('src', 'index.tsx'),
    ],
    output: {
        path: path.resolve('dist'),
        publicPath: '/',
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.jsx', '.js', '.json' ],
        alias: {
            '~': path.resolve('src')
        },
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
        ],
    },
    plugins: [
        new HtmlPlugin({
            template: path.resolve('src', 'index.html'),
            inject: 'body',
        }),
    ],
});
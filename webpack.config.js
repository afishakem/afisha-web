const path = require('path');

const env = process.env.NODE_ENV || 'development';

module.exports = require(path.resolve('webpack', `webpack.${env}`));